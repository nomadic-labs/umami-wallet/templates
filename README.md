# Umami-wallet group templates

This projects holds the templates (in particular the `issue templates`) used accross the Umami-wallet projects.

Processes descriptions can be found on the [umami wiki](https://gitlab.com/nomadic-labs/umami-wallet/umami/-/wikis/home) :

* Change management & Change Advisory Board (_CAB_): https://gitlab.com/nomadic-labs/umami-wallet/umami/-/wikis/process/change-management-cab 
* Incident management: https://gitlab.com/nomadic-labs/umami-wallet/umami/-/wikis/process/incident%20management


## Contributing
You can suggest change by opening a merge request on this project.



## Contact
How to contact the umami team :
https://gitlab.com/nomadic-labs/umami-wallet/umami/-/wikis/FAQ/Ask-for-help
