import os
import gitlab
import datetime

print("INFO    CI_PROJECT_ID="+os.environ["CI_PROJECT_ID"])
print("INFO    GITLAB_TOKEN: "+os.environ["GITLAB_TOKEN"])
print("INFO    ISSUE_FILTER_LABEL: "+os.environ["ISSUE_FILTER_LABEL"])
print("INFO    STRINGS_TO_CHECK: "+os.environ["STRINGS_TO_CHECK"])
print("INFO    NEW_LABEL: "+os.environ["NEW_LABEL"])
print("INFO    GRACE_PERIOD: "+os.environ["GRACE_PERIOD"])

# Get the grace period (Number of days after last note to post a message)
grace_period = int(os.environ["GRACE_PERIOD"])

# Initialize a GitLab client
gl = gitlab.Gitlab("https://gitlab.com", private_token=os.environ["GITLAB_TOKEN"])

# Get the list of project IDs
project_ids = os.environ["PROJECT_IDS"].split(",")

# Check the strings in descriptions
strings_to_check = os.environ["STRINGS_TO_CHECK"].split(",")
new_label = os.environ["NEW_LABEL"]

# Get the label to check
issue_filter_label = os.environ["ISSUE_FILTER_LABEL"]

for project_id in project_ids:
    # Get the project
    project = gl.projects.get(project_id)

    print(f"INFO {project_id} {project.web_url}")

    # Get open issues with label ISSUE_FILTER_LABEL
    issues = project.issues.list(state="opened", labels=[issue_filter_label])
    for issue in issues:
        issue_description = issue.description
        strings_present = True
        missing_strings = []

        for string_to_check in strings_to_check:
            if string_to_check not in issue_description:
                missing_strings.append(string_to_check)
                strings_present = False
                print(f"INFO {issue.web_url} missing {string_to_check}\n")

        if not strings_present:
            # Get the latest note from the issue
            latest_note = issue.notes.list(get_all=True)[0]
            note_date = datetime.datetime.strptime(latest_note.created_at, '%Y-%m-%dT%H:%M:%S.%fZ')
            #debug        
            # print(*missing_strings, sep = ", ")

            if (datetime.datetime.now() - note_date).days > grace_period:
                # Add a comment if the latest note is older than x days
                missing_strings_str = ', '.join(missing_strings)
                issue.notes.create({'body' : "The following are missing: " + missing_strings_str.replace('[x]','')+ " (and no activity has been recorded on the last "+str(grace_period)+" days )" } )
        
        if strings_present:
            issue.labels.append(new_label)
            issue.save()
            
            # Get the usernames of all assignees
            assignee_ids = [assignee.get("id") for assignee in issue.assignees]
            assignee_usernames = []
            for assignee_id in assignee_ids:
                user = gl.users.get(assignee_id)
                assignee_usernames.append("@" + user.username)
            assignee_usernames = ', '.join(assignee_usernames)

            issue.notes.create({'body' : "This is issue is now labeled ~"+new_label+f"\n Current assignees can proceed: {assignee_usernames}" } )
