[[_TOC_]]
## Scheduled change
/label ~"Change::Scheduled" <!-- Scheduled change, to be approved by the CAB before applying, and performed on the indicated due date. -->

<!-- A scheduled change is usually set on external dependencies. -->

<!-- Set Due Date : /due <in 2 days | this Friday | December 31st> -->
/due xxx

<!-- /confidential -->
<!-- If confidential, explain why -->

### Summary
<!-- Outline the issue being faced, and why this required a change !-->

### Area of the system
<!-- This might only be one part, but may involve multiple sections !-->

### How does this currently work?
<!-- The current process, and any associated business rules !-->

### What is the desired way of working?
<!-- After the change, what should the process be, and what should the business rules be !-->

<!-- Success criteria of change application (when relevant, include how to test) -->

### Change Procedure
- [ ] Change procedure been tested successfully

<!-- Include step by step description -->

## Rollback plan
<!-- Describe how to rollback the change in case the expected change is not working -->







<!-- METADATA for project management, please leave the following lines and edit as needed -->
# Metadata
<!-- PRIORITY: Uncomment /label quick actions as appropriate. The priority and severity assigned may be different to this !-->
<!--High : (This will bring a huge increase in performance/productivity/usability, or is a legislative requirement)-->
<!-- /label ~"Priority::1-High" -->
<!--Medium : (This will bring a good increase in performance/productivity/usability)-->
<!-- /label ~"Priority::2-Medium" -->
<!--Low : (anything else e.g., trivial, minor improvements) -->
<!--  /label ~"Priority::3-Low" -->

<!-- Reviewers : check the box [x], you may also add you @user handle  -->
## Approvals checklist(all required) 
- [ ] Approval from Development
- [ ] Approval from Operations
- [ ] Approval from Business 
<!-- tick the box [x], you may also add your @user handle at the end of the line -->

<!-- Trigger gitlab todo tasks --> 

@sagotch @leoparis89    Please *approve* this _scheduled change_ on development aspects

@comeh (cc: @philippewang.info) Please *approve* this _scheduled change_ on operations  aspects

@bsall                   Please *approve* this _scheduled change_ on business    aspects

<!-- comment next line if writing a draft -->
/assign @sagotch @leoparis89 @comeh @bsall @philippewang.info

<!-- Quick actions for last approver : -->
<!-- /unlabel ~"CAB::to-approve" -->
<!-- /label ~"CAB::to-perform"   -->

/label ~Change ~"CAB::to-approve" <!-- labels for gitlab CAB Change issues management -->

<!-- METADATA - end -->
