/label ~"type::Bug"  ~dev 
[[_TOC_]]
<!-- Thank you for opening a bug report to Umami -->
<!-- Please indicate as much details as possible so that we can reproduce the bug -->
# Bug description
# What did you expected 

# What did you observed

# Steps to reproduce

# Metadata
* Umami Version: 
* Tezos Network: <!-- if relevant : mainnet, testnet, ... -->

